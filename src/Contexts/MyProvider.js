import {MyContext} from "./MyContext";

const {Component} = require("react");

export class MyProvider extends Component {
    state = {
        clientName: "",
        clientFruit: 0,
        clientBirthday: new Date()
    };

    render() {
        return (
            <MyContext.Provider
                value={{
                    clientName: this.state.clientName,
                    clientFruit: this.state.clientFruit,
                    clientBirthday: this.state.clientBirthday,
                    setClientName: name => {
                        this.setState({
                            clientName : name
                        });
                    },
                    setClientFruit: fruit => {
                        this.setState({
                            clientFruit : fruit
                        });
                    },
                    setClientBirthday: date => {
                        console.log({date});
                        this.setState({
                            clientBirthday : date
                        });
                    }
                }}
            >
             {this.props.children}
            </MyContext.Provider>
        );
    }
}