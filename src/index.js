import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import {MyFormScreen} from './containers/MyFormScreen';


ReactDOM.render(
    <div>
        <MyFormScreen />
    </div>,
    document.getElementById('root')
);