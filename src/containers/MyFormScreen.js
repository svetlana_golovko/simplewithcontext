import React, {useState} from 'react';
import './MyFormScreen.css';

import {NameView} from '../components/NameView';
import {FruitView} from '../components/FruitView';
import {OutputView} from '../components/OutputView';
import {MyProvider} from "../Contexts/MyProvider";


export function MyFormScreen () {
        return (
            <div>
                <MyProvider >
                        <NameView />
                        <FruitView />
                        <OutputView />
                </MyProvider>
            </div>
        );
}
