import React from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {MyContext} from "../Contexts/MyContext";
import {Text} from "react-native";
import * as constants from "./constants";

export function BirthdayView() {
    return (
        <MyContext.Consumer>
            {context => {
               const nameInStyle = <Text style={constants.styles.bigBlue}>{context.clientName}</Text>
               return  <fieldset>
                    <legend> Select {nameInStyle} your birthday:</legend>
                    <DatePicker selected={context.clientBirthday}
                                onChange={(value, event) => {
                                    context.setClientBirthday(value)
                                }}
                                dateFormat="dd-MM-yyyy"/>
                </fieldset>
            }
            }
        </MyContext.Consumer>
    );
}

