import { StyleSheet } from "react-native";

export const FRUIT_OPTIONS = [
    {label: "", value: "0",},
    {label: "Apple", value: "1",},
    {label: "Mango", value: "2",},
    {label: "Banana", value: "3",},
    {label: "Pineapple", value: "4",},
];


export const styles = StyleSheet.create({
    container: {
        marginTop: 50,
    },
    bigBlue: {
        color: 'blue',
        fontWeight: 'bold',
    },
    red: {
        color: 'red',
    },
});