import React from 'react';
import {Text} from "react-native";
import * as constants from './constants';
import {format} from "date-fns";
import {MyContext} from "../Contexts/MyContext";


export function OutputView() {

    return (
        <MyContext.Consumer>
            {context => {
                const birthDayFormatted = format(context.clientBirthday, "dd-MM-yyyy");
                const selectedFruitName = (context.clientFruit === "0") ? " not selected" :
                    constants.FRUIT_OPTIONS
                        .filter((option) => option.value === context.clientFruit)
                        .map((option) => option.label);

                return <fieldset>
                    <legend>Dear Client:</legend>
                    Your name is <Text style={constants.styles.bigBlue}>{context.clientName}</Text> <p/>
                    Your favourite fruit is <Text style={constants.styles.bigBlue}>{selectedFruitName}</Text> <p/>
                    Your birthday is on <Text style={constants.styles.bigBlue}>{birthDayFormatted}</Text>
                </fieldset>
            }
            }
        </MyContext.Consumer>
    );
}