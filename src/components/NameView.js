import React, {useContext} from 'react';
import {BirthdayView} from './BirthdayView';
import {MyContext} from "../Contexts/MyContext";

export function NameView() {

    return (
        <MyContext.Consumer>
            {context => (
                <fieldset>
                    <legend>You personal data:</legend>
                    What is your name:
                    <input type="text"
                           placeholder="Enter you name:"
                           value={context.clientName}
                           onChange={(event) => {
                               console.log(event.target.value);
                               context.setClientName(event.target.value)
                           }
                           }
                    />
                    <BirthdayView />
                </fieldset>
            )}
        </MyContext.Consumer>
    );

}