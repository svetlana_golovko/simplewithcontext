import React, {useContext} from 'react';
import * as constants from './constants';
import {Text} from "react-native";
import {MyContext} from "../Contexts/MyContext";

export function FruitView() {

    return (
        <MyContext.Consumer>
            {context => {
                const nameInStyle = <Text style={constants.styles.bigBlue}>{context.clientName}</Text>

                   return <fieldset>
                        <legend>What is {nameInStyle} your favourite fruit:</legend>
                        <select value={context.clientFruit} onChange={(e) => {
                            context.setClientFruit(e.target.value)
                        }}>
                            {constants.FRUIT_OPTIONS.map((option) => (
                                <option key={option.value} value={option.value}>{option.label}</option>
                            ))}
                        </select>
                    </fieldset>

            }


            }
        </MyContext.Consumer>
    );
}
